import { Controller, Get, Header, Query, StreamableFile } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('api/v1.0')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('generate')
  @Header('Content-Type', 'image/jpeg')
  async generateImage(
    @Query('title') title: string,
    @Query('description') description: string,
    @Query('image') image: string,
  ): Promise<StreamableFile> {
    return this.appService.getImage({ title, description, image });
  }
}
