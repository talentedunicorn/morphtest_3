import {
  Injectable,
  InternalServerErrorException,
  Logger,
  StreamableFile,
} from '@nestjs/common';
import puppeteer from 'puppeteer';

interface Request {
  title: string;
  description: string;
  image: string;
}

@Injectable()
export class AppService {
  private readonly logger: Logger = new Logger(AppService.name);

  async getImage({ title, description, image }: Request) {
    this.logger.log('Generating image...');
    const browser = await puppeteer.launch({
      args: ['--no-sandbox'],
    });
    const page = await browser.newPage();
    page.setViewport({ width: 700, height: 400 });

    try {
      await page.goto(
        `http://localhost:3000/?title=${encodeURIComponent(title)}&description=${encodeURIComponent(description)}&image=${encodeURIComponent(image)}`,
      );

      const screenshot = await page.screenshot({ type: 'jpeg' });
      return new StreamableFile(screenshot);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e);
    } finally {
      await browser.close();
      this.logger.log('Done!');
    }
  }
}
