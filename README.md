## Morph test 3

Backend Microservice / REST API test (Advanced)


## Requirements
1. Create a microservice REST API with routing/controller/version such as:
  a. Eg: your_domain/dynamicimage/api/v1.0/generate
1. This API will be have GET parameters appended to url above as part of the request with the following fields
   - Image Url -> this will be used as background image
   - Title
   - Description
1. From GET params in 2, generate a dynamic image using html/css and return API response/output in “image/jpeg” format


### Requirement:
- Spacing A and spacing B (refer to image) must be dynamically adjusted according to content height. The top and bottom spaces should readjust to dynamically centre the content regardless of content height. Image size should be 700px by 400px. Title has to be red colour.
- Use NodeJS, EJS, Puppeteer, HTML, CSS (hint: Bootstrap 5)
- This application should be Dockerized into a Dockerfile

## Installation

```bash
$ pnpm install
```

## Running the app

### Docker
```bash
# Build an image using the Dockerfile
$ docker build -t morphtest3 .

# For ARM-based CPU i.e. Macbook M1
$ docker build --platform linux/amd64 -t morphtest3 .

# Run a container and map port 3000
$ docker run --name morphtest -dp 3000:3000 morphtest3

# Now you can call GET http://localhost:3000 to get response
$ curl http://localhost:3000/api/v1.0/generate?title=TITLE&description=DESCRIPTION&image=IMAGE
```

### Without Docker

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## License

Nest is [MIT licensed](LICENSE).
