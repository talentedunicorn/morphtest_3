const { defineProps } = Vue;

const Header = {
  setup() {
    defineProps({
      title: '',
      content: ''
    })
  },
  template: `
    <header>
      <h1>{{ title }}</h1>
      <p>{{ content }}</p>
    </header>
  `,
}

export default Header;