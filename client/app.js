let app

const init = () => {
  app = document.querySelector('#app')
  const { title, description, image } = getQueryParams()
  if (!title || !description || !image) return;
  assignContent(title, description, image)
}

document.addEventListener('DOMContentLoaded', init)

const getQueryParams = () => {
  const params = new URLSearchParams(document.location.search);
  const title = params.get('title')
  const description = params.get('description')
  const image = params.get('image')

  return { title, description, image }
}

const assignContent = (title, description, image) => {
  const titleEl = app.querySelector('h1')
  const descriptionEl = app.querySelector('p')

  app.style.setProperty("--bg", `url(${image})`)

  titleEl.innerText = title;
  descriptionEl.innerText = description;
}